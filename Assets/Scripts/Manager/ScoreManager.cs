﻿using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        public static ScoreManager Instance { get; private set; }

        public void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this.gameObject);
                return;
            }

            if (Instance != null) return;
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
        }
    }
}


