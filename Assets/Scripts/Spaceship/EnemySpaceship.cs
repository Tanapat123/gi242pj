using System;
using Manager;
using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{
    public class EnemySpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
        }

        private void Start()
        {
            FullHp = Hp;
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioManager.Instance.Play("Explosion1");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
            Invoke(nameof(OnDestroy), 0.05f);
        }
        
        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, gunPosition.rotation);
            AudioManager.Instance.Play("Laser2");
            bullet.Init();
        }
        
        private void OnDestroy()
        {
            Destroy(gameObject);
        }
    }
}